view: testing {
  derived_table: {
    sql_trigger_value: SELECT COUNT(*) FROM ${derived1.SQL_TABLE_NAME} ;;
    distribution_style: all
    sql: -- raw sql results do not include filled-in values for 'user_order_facts.first_order_date'


            -- use existing user_order_facts in davidleftwich_scratch.LR$F8P621606430466630_user_order_facts
            SELECT
              DATE(user_order_facts.first_order ) AS "user_order_facts.first_order_date",
              COUNT(DISTINCT users.id ) AS "users.count"
            FROM public.order_items  AS order_items
            LEFT JOIN public.users  AS users ON order_items.user_id = users.id
            LEFT JOIN davidleftwich_scratch.LR$F8P621606430466630_user_order_facts AS user_order_facts ON user_order_facts.user_id = order_items.user_id

            GROUP BY 1
            ORDER BY 1 DESC
            LIMIT 500
             ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: user_order_facts_first_order_date {
    type: date
    sql: ${TABLE}."user_order_facts.first_order_date" ;;
  }

  dimension: users_count {
    type: number
    sql: ${TABLE}."users.count" ;;
  }

  set: detail {
    fields: [user_order_facts_first_order_date, users_count]
  }
}
