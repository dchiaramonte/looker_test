explore: derived1 {hidden:yes}

view: derived1 {
  derived_table: {
    distribution_style: all
    sql_trigger_value: SELECT DATE_PART('hour', GETDATE()) ;;
    sql: -- raw sql results do not include filled-in values for 'order_items.created_date'


            SELECT
              DATE(order_items.created_at ) AS "order_items.created_date",
              COUNT(DISTINCT order_items.id ) AS "order_items.count"
            FROM public.order_items  AS order_items

            GROUP BY 1
            ORDER BY 1 DESC
            LIMIT 500
             ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: order_items_created_date {
    type: date
    sql: ${TABLE}."order_items.created_date" ;;
  }

  dimension: order_items_count {
    type: number
    sql: ${TABLE}."order_items.count" ;;
  }

  set: detail {
    fields: [order_items_created_date, order_items_count]
  }
}
